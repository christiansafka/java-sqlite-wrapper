package DB;
import java.sql.*;
import java.util.Scanner;


public class DBhandler {

	public static void main(String[] args) {
		
		
		Connection c = null;
		Statement stmt = null;
	    try 
	    {
	      Class.forName("org.sqlite.JDBC");
	      c = DriverManager.getConnection("jdbc:sqlite:test.db");
	      c.setAutoCommit(false);
	      stmt = c.createStatement();
	      
	      //String sql = createTableString();  //just cleaned up the code a bit by using a function to build the string
	      //stmt.executeUpdate(sql);           //uncomment these lines to create the table
	      
	      System.out.println("Opened database successfully");
	      
	      /*
	      insert(stmt, 1, "Joe", 46, "56 Stanwick Rd.", 50000.00);   //using my insert function to add rows
	      insert(stmt, 2, "Bob", 23, "12 Stanwick Rd.", 10000.00);
	      insert(stmt, 3, "Mary", 32, "42 Stanwick Rd.", 20440.00);
	      */
	      
	      System.out.println("Enter SQL commands for table: COMPANY(ID, AGE, NAME, ADDRESS, SALARY)\n"
	      		+ "OR use 'FIND #' to lookup record from ID\n"
	      		+ "use EXIT to close connections and exit program!");
	      
	      Scanner in = new Scanner(System.in);
	      while(true)
	      {
	    	  String command = in.nextLine();
	    	  if(command.startsWith("FIND") == true)
	    	  {	 
	    		  getDataFromId(stmt, Integer.parseInt(command.split("\\s+")[1]));  
	    	  }
	    	  
	    	  else if(command.equals("EXIT") == true)
	    	  {
	    		  try
	    		  {
	    			  in.close();
	    			  stmt.close();
	    			  c.commit();
	    			  c.close();
	    			  System.exit(0);
	    		  }
	    		  catch (SQLException e)
	    		  {
	    			  System.out.println(e);
	    		  }
	    	  }
	    	  
	    	  else
	    	  {
	    		  execute(stmt, command);
	    	  }
	      } 
	      
	    } 
	    catch ( Exception e ) 
	    {
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      System.exit(0);
	    }
		
	}
	
	public static boolean insert(Statement stmt, int ID, String name, int age, String address, double salary)
	{
		String sql = "INSERT INTO COMPANY(ID, NAME, AGE, ADDRESS, SALARY)"
				+ " VALUES (" + ID + ", '" + name + "', " + age + ", '" + address + "', " + salary + ");";
	    
	    try 
		{
	    	stmt.executeUpdate(sql);
		}
		catch (Exception e)
		{
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			return false;
		}
		return true;
	}
	
	public static String createTableString()
	{
		String sql = "CREATE TABLE COMPANY " +
                "(ID INT PRIMARY KEY     NOT NULL," +
                " NAME           TEXT    NOT NULL, " + 
                " AGE            INT     NOT NULL, " + 
                " ADDRESS        CHAR(50), " + 
                " SALARY         REAL)"; 
		return sql;
	}
	
	public static void getDataFromId(Statement stmt, int ID)
	{
		try 
		{
			ResultSet rs = stmt.executeQuery("SELECT * FROM COMPANY WHERE ID = " + ID + ";");
			while ( rs.next() ) 
			{
		         int id = rs.getInt("id");
		         String  name = rs.getString("name");
		         int age  = rs.getInt("age");
		         String  address = rs.getString("address");
		         float salary = rs.getFloat("salary");
		         System.out.println( "ID = " + id );
		         System.out.println( "NAME = " + name );
		         System.out.println( "AGE = " + age );
		         System.out.println( "ADDRESS = " + address );
		         System.out.println( "SALARY = " + salary );
		         System.out.println();
		    }
		    rs.close();
			
		} 
		catch (SQLException e) 
		{
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		}
			
	}
	public static void execute(Statement stmt, String query)
	{
		try
		{
			try
			{
				ResultSet rs = stmt.executeQuery(query);
				int id = rs.getInt("id");
		        String  name = rs.getString("name");
		        int age  = rs.getInt("age");
		        String  address = rs.getString("address");
		        float salary = rs.getFloat("salary");
		        System.out.println( "ID = " + id );
		        System.out.println( "NAME = " + name );
		        System.out.println( "AGE = " + age );
		        System.out.println( "ADDRESS = " + address );
		        System.out.println( "SALARY = " + salary );
		        System.out.println();
		        rs.close();
			}
			catch(Exception e)
			{
				stmt.executeUpdate(query);
				System.out.println("Success\n");
			}
		}
		catch (SQLException e)
		{
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		}
		
	}
	

}
